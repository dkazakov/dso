
PRJ_DIR='dso_prj1'

mkdir -p $PRJ_DIR

cd $PRJ_DIR && 

python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install wheel
pip install --upgrade pip
python3 -m pip install -r ../dso_prj1_requirements.txt

django-admin startproject config

deactivate