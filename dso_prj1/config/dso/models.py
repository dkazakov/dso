from django.db import models


class User(models.Model):
    """Model representing a user and it's properties"""
    email = models.EmailField(default=None, null=True, blank=True, max_length=100, help_text='')
    phone = models.CharField(default=None, null=True, blank=True, max_length=30, help_text='')

    class Meta:
        verbose_name_plural = 'Пользователь'

    def __str__(self):
        """String for representing the Model object (in Admin site etc.)"""
        return self.id

