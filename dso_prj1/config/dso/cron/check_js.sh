#!/bin/bash

NOW=`date`
RESPONSE_CODE=`curl -s -o /dev/null -w "%{http_code}" --insecure -I https://fn.rt.ru/rtk.js`
echo -e "$NOW\t$RESPONSE_CODE"
