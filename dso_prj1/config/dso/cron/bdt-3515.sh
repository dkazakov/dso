#!/bin/bash

response=`curl --insecure -vL -b 'viuserid=12345678901234567890' 'https://fnc.rt.ru/1/6532/i/i?i=311815611177098.562918656288029' 2>&1 | grep Location`
#< Location: https://dmg.digitaltarget.ru/1/6533/i/i?i=926087001583418404352000000002885070&a=774&e=12345678901234567890

if [[ $response == *"https://dmg.digitaltarget.ru/1/6533/"* ]]; then
  echo "Response ok"
else
  echo "Error: $response"
fi