import uuid
import datetime
from django.urls import reverse
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.views.decorators.clickjacking import xframe_options_exempt
from rest_framework import status, generics
from .models import User
from .serializers import UserSerializer


#class UserContactsViewSet(viewsets.ModelViewSet):
#    """API endpoint that allows users to be viewed or edited."""
#    queryset = User.objects.all()
#    serializer_class = UserSerializer

class ApiUser(generics.GenericAPIView):
    """ API methods for user properties """

    serializer_class = UserSerializer

    def get(self, request, user_id):
        user = User.objects.get(id=user_id)
        res = {
            'id': user_id,
            'phone': user.phone,
            'email': user.email
        }
        return JsonResponse(res, status=status.HTTP_200_OK)

    def post(self, request, user_id):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(id=user_id)
            return JsonResponse({}, status=status.HTTP_201_CREATED)
        return JsonResponse({}, status=status.HTTP_400_BAD_REQUEST)


@xframe_options_exempt
def index(request):
    if not 'DSOUserID' in request.COOKIES:
        dso_user_id = str(uuid.uuid4())
        rendered = render_to_string('index.html', context={'DSOUserID': dso_user_id})
        response = HttpResponse(rendered)

        max_age = 365 * 86400
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")

        response.set_cookie('DSOUserID', dso_user_id, max_age=max_age, expires=expires)
    else:
        dso_user_id = request.COOKIES.get('DSOUserID')
        rendered = render_to_string('index.html', context={'DSOUserID': dso_user_id})
        response = HttpResponse(rendered)

    print('qq', request.headers, 'pp', request.COOKIES.get('DSOUserID'), 'ww', dso_user_id)
    return response

    #return render(
    #    request,
    #    'index.html',
    #    context={
    #    }
    #)

def index3(request):
    rendered = render_to_string('index3.html', context={}, )
    response = HttpResponse(rendered)
    return response


def tdm1(request):
    rendered = render_to_string('tdm1.html', context={}, )
    response = HttpResponse(rendered)
    return response

def tdm2(request):
    rendered = render_to_string('tdm2.html', context={}, )
    response = HttpResponse(rendered)
    return response

def tdm3(request):
    rendered = render_to_string('tdm3.html', context={}, )
    response = HttpResponse(rendered)
    return response






