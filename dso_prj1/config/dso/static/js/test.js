
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var get_user_id = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return 'dso__' + Math.random().toString(36).substr(2, 9);
};

var user_id = getCookie('test_dsouserid') || get_user_id();
document.cookie = "test_dsouserid=" + user_id + "; max-age=40176000";

