var get_random = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return 'mrg__' + Math.random().toString(36).substr(2, 9);
};
var script2 = document.createElement('script');
script2.src = "https://dmg.digitaltarget.ru/1/6724/i/i?i=" + get_random() + "&c=tg:campaign_MRGTEST";
document.body.appendChild(script2);