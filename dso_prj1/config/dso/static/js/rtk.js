function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
var get_user_id = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return 'dso2__' + Math.random().toString(36).substr(2, 15);
};
var script = document.createElement('script');
script.src = "https://static.terratraf.io/engine/GP.js";
document.body.appendChild(script);
var user_id = getCookie('dsouserid') || get_user_id();
document.cookie = "dsouserid=" + user_id + "; max-age=40176000";
window.generalPixel = window.generalPixel || [];
window.generalPixel.push({type:'GPID', id:'10001F44'});
window.generalPixel.push({UserID: user_id});
var adcm_config ={
    id: 7120,
    tags: [user_id]
};
var script2 = document.createElement('script');
script2.src = "https://tag.digitaltarget.ru/adcm.js";
document.body.appendChild(script2);