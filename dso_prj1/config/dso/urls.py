"""dso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/', include((router.urls, 'api'), namespace='api'), ),
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/docs/', get_swagger_view(title='DSOFirstPrjAPI'), ),

    path('api/user/<str:user_id>', views.ApiUser.as_view(), name='api-user'),

    path('', views.index, name='main'),
    path('index3', views.index3, name='index3'),

    # tdm
    path('tdm1', views.tdm1, name='tdm1'),
    path('tdm2', views.tdm2, name='tdm2'),
    path('tdm3', views.tdm3, name='tdm3'),
]
